Пояснение для модуля binvox_rw.py
Распаковать архив binvox-rw-py[github]
Создать в этой папке файл test.py со следующим содержимым:

import numpy as np
import binvox_rw
with open('chair.binvox', 'rb') as f:  # rb - режим открытия файла r- read
     m1 = binvox_rw.read_as_3d_array(f)
print "As 3D array"
print m1.data, "\n\n\n\n"
print "As coord array"
with open('chair.binvox', 'rb') as f:
    m1 = binvox_rw.read_as_coord_array(f)
print m1.data

В этом коде читается файл с расширением *.binvox (с применением функций из модуля binvox_rw) и выводятся данные в виде 3д массива и в видео координат.

Для обратно операции - записи данных в бинарный формат binvox используется метод write из модуля binvox_rw:

with open('chair_out.binvox', 'wb') as f: # w - режим записи файла write
     m1.write(f)  # метод записи 

При этом создается новый файл chair_out.binvox если он не был создан


Для проверки на правильность записи данных в исходном и созданном файлах используется следующий код:

print m1.scale

print m1.translate
with open('chair_out.binvox', 'wb') as f:
     m1.write(f)

with open('chair_out.binvox', 'rb') as f:
     m2 = binvox_rw.read_as_3d_array(f)

print m1.dims==m2.dims
print m1.scale==m2.scale
print m1.translate==m2.translate
print np.all(m1.data==m2.data)
with open('chair.binvox', 'rb') as f:
     md = binvox_rw.read_as_3d_array(f)
with open('chair.binvox', 'rb') as f:
     ms = binvox_rw.read_as_coord_array(f)

data_ds = binvox_rw.dense_to_sparse(md.data)
data_sd = binvox_rw.sparse_to_dense(ms.data, 32)
np.all(data_sd==md.data)
np.all(ms.data[:, np.lexsort(ms.data)] == data_ds[:, np.lexsort(data_ds)])

