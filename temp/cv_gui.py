#!/usr/bin/python
# -*- coding: cp1251 -*-

from PyQt4 import QtGui
from PyQt4 import QtCore
import cv2
import img_proc as im_proc
import table


class NewWidget(QtGui.QWidget):
    def __init__(self, parent=None):
        QtGui.QWidget.__init__(self, parent)
        self.initUI()

    def initUI(self):
        self.setWindowIcon(QtGui.QIcon('icons/ico3.png'))
        self.setWindowTitle('Computer Vision System GUI')

        self.setToolTip('Developed by <b>Terekhin A.V.</b> Murom {0} 2016'.format(chr(169)))
        self.setGeometry(500, 500, 400, 200)

        self.show()


class cvGUI(QtGui.QWidget):
    def __init__(self, parent=None):
        QtGui.QWidget.__init__(self, parent)
        self.initUI()
        # QString To QByteArrray - b = qs.toUtf8
        # QByteArray to string - s = str(b)
        #
        #
        # ����������� �������� ����
        #
        #
    def initUI(self):
        self.setWindowIcon(QtGui.QIcon('icons/ico3.png'))
        self.setWindowTitle('Computer Vision System GUI')

        self.setToolTip('Developed by <b>Terekhin A.V.</b> Murom {0} 2016'.format(chr(169)))
        QtGui.QToolTip.setFont(QtGui.QFont('OldEnglish', 10))        
        

        self.format_list = ["jpg", "bmp", "png"]        

        # �� ������ ������
        self.center()
        #
        #
        # ��������� ����������
        #
        #
        # ����� ����
        self.filename = ""
        self.size = 200
        self.new_opened = False
        self.fileslist = ""
        # self.frame = ""
        self.fcap = False
        # ��� �������/�������� ����
        self.step = 0
        #
        # ��� �����
        #
        self.fps = 24
        self.n = 1
        self.list_of_cams = {}

        self.get_cams()

        print self.list_of_cams
        self.cap = None #cv2.VideoCapture(self.n)
        self.cap2 = None

        self.capture = None
        self.start_button = QtGui.QPushButton('Start')
        self.start_button.clicked.connect(self.startCapture)
        self.quit_button = QtGui.QPushButton('End')
        self.quit_button.clicked.connect(self.endCapture)
        self.end_button = QtGui.QPushButton('Stop')
        #
        #
        # ������� ������� ����������� �����
        #
        #
        self.image = QtGui.QLabel('', self)
        self.image2 = QtGui.QLabel('', self)
        self.default_image()

        # ������� ������
        # ������ �������� ����� (�����������)
        self.btnOpen = QtGui.QPushButton('Open', self)
        self.btnOpen.clicked.connect(self.OpenDialog)
        self.btnOpen.setFocusPolicy(QtCore.Qt.NoFocus)
        # ������ �������� ��������� ����
        self.btnNewWin = QtGui.QPushButton('New window', self)
        self.btnNewWin.clicked.connect(self.addwindow)
        self.btnNewWin.setFocusPolicy(QtCore.Qt.NoFocus)
        # ������ ����������� ������ �� ������ TextEdit � ������
        self.btnMove = QtGui.QPushButton('MoveText', self)
        self.btnMove.clicked.connect(self.on_movetext)
        self.btnMove.setFocusPolicy(QtCore.Qt.NoFocus)
        #
        # self.btnVideo = QtGui.QPushButton('start video', self)
        # self.btnVideo.clicked.connect(self.on_capture)
        # self.btnVideo.setFocusPolicy(QtCore.Qt.NoFocus)
        # progress
        # ������ ������� �������� ����
        self.btnstart = QtGui.QPushButton('Start', self)
        self.btnstart.clicked.connect(self.on_progress)
        self.btnstart.setCheckable(True)
        self.btnstart.setFocusPolicy(QtCore.Qt.NoFocus)
        # �������
        self.cb = QtGui.QCheckBox('Show filename', self)
        self.cb.stateChanged.connect(self.on_checkbox)
        self.cb.setFocusPolicy(QtCore.Qt.NoFocus)
        # TextEdits
        self.reviewEdit = QtGui.QTextEdit()
        self.aboutEdit = QtGui.QTextEdit()
        # �������
        self.slider = QtGui.QSlider(QtCore.Qt.Horizontal, self)
        self.slider.valueChanged.connect(self.on_slide)
        self.slider.setRange(self.size, self.size+500)
        self.slider.setFocusPolicy(QtCore.Qt.NoFocus)
        # �����������
        self.pbar = QtGui.QProgressBar(self)
        self.pbar.setGeometry(30, 40, 200, 25)
        # ��������� ������
        self.timer = QtCore.QBasicTimer()
        self.video_timer = QtCore.QTimer()

        # ������
        self.list = QtGui.QListWidget(self)
        self.list.itemClicked.connect(self.on_item_select)

        # ������� �������
        # self.view = QtGui.QTableView(self)
        # self.model = QtGui.QStandardItemModel(self)
        # for rowName in range(3) * 5:
        #     self.model.invisibleRootItem().appendRow(
        #         [   QtGui.QStandardItem("row {0} col {1}".format(rowName, column))
        #             for column in range(3)
        #             ]
        #         )
        # self.proxy = QtGui.QSortFilterProxyModel(self)
        # self.proxy.setSourceModel(self.model)
        # self.view.setModel(self.proxy)


        # ������ �������
        self.header = ['First', 'Second', 'Third']
        self.tabledata = ['', '', '']

        self.get_table_data()
        self.table = self.createTable()

        #
        #
        # ������� ����� ��� ���������� ��������
        #
        #
        grid = QtGui.QGridLayout()
        grid.setSpacing(10)

        grid.addWidget(self.btnOpen, 1, 0)
        # grid.addWidget(self.btnVideo, 1, 4)
        grid.addWidget(self.start_button, 1, 5)
        grid.addWidget(self.end_button, 2, 5)
        grid.addWidget(self.quit_button, 3, 5)
        grid.addWidget(self.btnNewWin, 2, 4)
        grid.addWidget(self.btnMove, 2, 0)
        grid.addWidget(self.reviewEdit, 4, 0, 1, 4)
        grid.addWidget(self.aboutEdit, 4, 4, 1, 4)
        grid.addWidget(self.image, 0, 0, 1, 4)
        grid.addWidget(self.image2, 0, 4, 1, 4)
        grid.addWidget(self.list, 0, 9, 1, 2)
        # grid.addWidget(self.view, 0, 11, 5, 60)
        grid.addWidget(self.table, 0, 11, 5, 60)
        grid.addWidget(self.cb, 5, 0)
        grid.addWidget(self.btnstart, 6, 0)
        grid.addWidget(self.pbar, 7, 0, 1, 8)
        grid.addWidget(self.slider, 8, 0, 1, 8)

        self.setLayout(grid)
        self.resize(300, 300)
    # ������ ������ cvGui

    def default_image(self):
        self.img = cv2.imread(r'no_img.png')

        if self.img is not None:
            self.cvImage = self.img
            self.RIM = im_proc.im_res(self.img, 250)

            height, width, bytesPerComponent = self.RIM.shape
            bytesPerLine = 3 * width

            self.img = cv2.cvtColor(self.RIM, cv2.COLOR_BGR2RGB)
            QImg = QtGui.QImage(self.img.data, width, height, bytesPerLine,QtGui.QImage.Format_RGB888)
            pixmap = QtGui.QPixmap.fromImage(QImg)

        if pixmap != "":
            self.image.setAlignment(QtCore.Qt.AlignCenter)
            self.image2.setAlignment(QtCore.Qt.AlignCenter)
            self.image.setPixmap(pixmap)
            self.image2.setPixmap(pixmap)

    def center(self):
        screen = QtGui.QDesktopWidget().screenGeometry()
        size =  self.geometry()
        self.move((screen.width()-size.width())/2, (screen.height()-size.height())/2)

    def OpenDialog(self):
        try:
            if self.cap:
                self.deleteLater()
            self.fileslist = QtGui.QFileDialog.getOpenFileNames(self, 'Open file',
                        '/home')
            self.frmlist = []
            print "List of available image formats:{0}".format(self.format_list)
            if self.fileslist is not None:
                if len(self.fileslist) > 1:
                    print "Opening {} files...".format(len(self.fileslist))
                for i in self.fileslist:
                    self.new_opened = True
                    self.list.clear()
                    self.add_item(self.fileslist)


                    # self.table = self.createTable(self.table)
                    # print len(self.fileslist)

                    self.filename = str(i.toUtf8())
                    self.frmlist.append(str(i.split("/")[-1].split(".")[-1]))

                    print "You try to open file with format: {0}".format(self.frmlist.pop())
                    print "    Opening file {0}...".format(self.filename)

                    # print "\n\n{0}\n\n{1}".format(type(str(i.toUtf8())), type(i.toUtf8()))
                    self.show_img(i.toUtf8())

                    frm = str(i.split("/")[-1].split(".")[-1])
                    fn = i.split("/")[-1]
                    self.list.setCurrentRow(self.list.count()-1)

                    if frm not in self.format_list:
                        self.list.clear()
                        print "Failed to open file \"{0}\". Wrong file format: [\'{1}\']".format(str(fn), frm)
                        self.fileslist = None
                        break
                # self.frame = None

        except Exception as e:
            print e.args, e.message

    def show_img(self, fname=None):#, frame=None):
        if fname is not None: #and frame is None:
            frm = fname.split("/")[-1].split(".")[-1]
            fp = str(fname).replace("/", "//")
            if fp is not None and frm in self.format_list:
                if self.new_opened == True:
                    print "    File \"{0}\" successfully opened".format(self.filename)
                pm = self.on_pixmap(fp, self.size)[0]
                pm2 = self.on_pixmap(fp, self.size)[1]
                self.image.setAlignment(QtCore.Qt.AlignCenter)
                self.image2.setAlignment(QtCore.Qt.AlignCenter)
                """����� �� QLable ������������� QPixmap"""
                self.image.setPixmap(pm)
                self.image2.setPixmap(pm2)

            else:
                self.filename = ""
                self.default_image()
                if self.new_opened == True:
                    print "Cannot show image \"{0}\". Reason: Wrong file format:\"{1}\"".format(str(fname.split("/")[-1]), frm)
        # elif frame is not None and fname is None:
        #     pm = self.setup_pixmap(frame)
        #     self.image.setPixmap(pm)
        #     self.image2.setPixmap(pm)

    def setup_pixmap(self, img):
        if len(img.shape)>2:
            """���� �������"""
            h, w, bpLine = img.shape
        else:
            """���� �����������"""
            img2 = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)
            h, w, bpLine= img2.shape        #����������
        if img is not None:
            """bpLine = byte per line"""
            bpLine = 3 * w
            """����� ���������� QImage �� IplImage � ��������� ����� ��������"""
            qim = QtGui.QImage(img.data, w, h, bpLine, QtGui.QImage.Format_RGB888)
            pm = QtGui.QPixmap.fromImage(qim)
            return pm

    def on_pixmap(self, path, size):
        if path != "":
            """����� � cvImage ����������� �����������"""
            cvImage = im_proc.im_op(path)

            if cvImage is not None:
                """����� ���������� ��������������"""
                bgr_gray = im_proc.im_gray(cvImage)
                bgr_blur = im_proc.im_mblur(cvImage, 5)

                # bgr = cv2.cvtColor(cvImage, cv2.COLOR_BGR2RGB)
                rgb_gray = im_proc.im_gray2rgb(bgr_gray)
                rgb_blur = im_proc.im_bgr2rgb(bgr_blur)

                """����� ���������� ������"""
                res_gray = im_proc.im_res(rgb_gray, size)
                res_blur = im_proc.im_res(rgb_blur, size)
                return self.setup_pixmap(res_gray), self.setup_pixmap(res_blur)

    def on_movetext(self):
        text = self.aboutEdit.toPlainText()
        # print type(text)
        self.reviewEdit.setText(text)
        self.aboutEdit.setText("")

    def on_checkbox(self, value):
        if self.cb.isChecked():
            if len(self.fileslist) > 1 and self.list.currentItem().text() != "" and self.cap.read()[1] is None:
                self.setWindowTitle(self.list.currentItem().text().split("/")[-1])
            elif len(self.fileslist) == 1 and self.cap.read()[1] is None:
                self.setWindowTitle(str(self.fileslist[0]).split("/")[-1])
            elif len(self.fileslist) == 0 and self.cap.read()[1] is None:
                self.setWindowTitle('Image not found')
            elif self.cap.read()[1] is not None:
                self.setWindowTitle('Capturing video from cam')
            else:
                self.setWindowTitle('Computer Vision System GUI')
        else:
            self.setWindowTitle('Computer Vision System GUI')

    def on_slide(self):
        pos = self.slider.value()
        self.reviewEdit.setText(str(pos))
        self.on_resize()

    def timerEvent(self, event):
        if self.step >= 100:
            self.timer.stop()
            self.step = 0
            self.btnstart.setText('Start')
            self.btnstart.setCheckable(False)
            self.btnstart.setCheckable(True)
            return        
        self.step += 1
        self.pbar.setValue(self.step)

        # self.on_capture()

    def on_progress(self):
        if self.timer.isActive():
            self.btnstart.setCheckable(True)
            self.timer.stop()
            self.btnstart.setText('Start')
        elif self.timer.isActive() == False and self.step != 100:
            self.timer.start(100, self)
            self.btnstart.setText('Stop')
        else:
            self.btnstart.setCheckable(True)
            self.step = 0
            self.btnstart.setText('Start')
        pass

    def add_item(self, flist):
        if len(flist) > 1:
            try:
                for string in flist:
                    self.list.addItem(string)
            except Exception as e:
                print e.args, e.message

    def on_item_select(self):
        self.new_opened = False
        if self.cb.isChecked():
            self.setWindowTitle(self.list.currentItem().text().split("/")[-1])
        self.stop()
        self.show_img(str(self.list.currentItem().text().toUtf8()))


    def on_resize(self):
        self.size = int(self.slider.value())
        # !!!!!!!! ���� ������ ���� - ��������� �������� �����������
        if len(self.fileslist) > 1 and not self.cap:# and self.frame is None:
            # print len(self.fileslist)
            self.new_opened = False
            if self.slider.value() != "" and self.new_opened == False:
                self.size = int(self.slider.value())

                self.show_img(str(self.list.currentItem().text().toUtf8()))
                # self.show_img(str(self.filename))
            self.new_opened = False
        elif self.cap:
            self.size = int(self.slider.value())
            # self.show_img(None, self.frame)
            self.new_opened = False
        # elif self.frame is None and self.fileslist == 0:
        #     pass
        else:
            # print len(self.fileslist)
            if self.slider.value() != "" and self.new_opened is False and len(self.fileslist) != 0:
                self.size = int(self.slider.value())
                self.show_img(self.fileslist[0].toUtf8())
                # self.show_img(str(self.filename))
            self.new_opened = False

    def addwindow(self):
        #""" C++: void foo() """
        self.addwin = NewWidget()

    # ������ ������� �� ���������� �������
    def get_table_data(self):
        # stdouterr = os.popen4("dir /home/")[1].read()
        # lines = stdouterr.splitlines()
        # lines = lines[5:]
        # lines = lines[:-2]
        # self.tabledata = [re.split(r"\s+", line, 4)
        #              for line in lines]


        # if len(self.fileslist) > 1:
        #     try:
        #         self.header = [self.fileslist[0].split("/")]
        #         for s in self.fileslist:
        #             self.tabledata.append(s.split("/"))
        #     except Exception as e:
        #         print e.args, e.message
        self.tabledata = [['1', '1', '1'],
                          ['2', '1', '1'],
                          ['3', '1', '1'],
                          ['4', '1', '1']]

    def createTable(self):
        # create the view
        tv = QtGui.QTableView()
        # tv = table

        # set the table model

        tm = table.MyTableModel(self.tabledata, self.header, self)
        tv.setModel(tm)

        # set the minimum size
        tv.setMinimumSize(400, 300)

        # hide grid
        tv.setShowGrid(False)

        # set the font
        font = QtGui.QFont("Courier New", 8)
        tv.setFont(font)

        # hide vertical header
        vh = tv.verticalHeader()
        vh.setVisible(False)

        # set horizontal header properties
        hh = tv.horizontalHeader()
        hh.setStretchLastSection(True)

        # set column width to fit contents
        tv.resizeColumnsToContents()

        # set row height
        nrows = len(self.tabledata)
        for row in xrange(nrows):
            tv.setRowHeight(row, 18)

        # enable sorting
        tv.setSortingEnabled(True)

        return tv

    # ������ ������� ��� ���������� ����� ������
    # TODO ������� � ��������� �����, ������� ����������� �� QLabel

    # def get_cams(self):
    #     for i in xrange(10):
    #         try:
    #             self.tmpcap = cv2.VideoCapture(i)
    #             k = self.tmpcap.isOpened()
    #             self.list_of_cams[i] = k
    #
    #         except Exception as e:
    #             print e.args, e.message
    #     self.tmpcap.release()

    def startCapture(self):
        if not self.capture:
            self.end_button.clicked.connect(self.stop)

            # self.capture.setFPS(1)
        if not self.cap:
            self.free_cam_list = [i for i in self.list_of_cams if self.list_of_cams[i]]
            if self.free_cam_list:
                self.cap = cv2.VideoCapture(self.free_cam_list.pop())
                self.cap2 = cv2.VideoCapture(self.free_cam_list.pop()) if self.free_cam_list else self.cap
        if self.cap.isOpened():
            self.start()
        else:
            print "No capture..."

    def endCapture(self):
        self.deleteLater()

    def setFPS(self, fps):
        self.fps = fps

    def nextFrameSlot(self):
        # try if cap is not None
        try:
            if self.cap.read()[1] is not None:
                ret, self.frame = self.cap.read()
                ret2, self.frame2 = self.cap.read()
                # My webcam yields frames in BGR format
                self.orig_frame = cv2.cvtColor(self.frame, cv2.cv.CV_BGR2RGB)
                self.frame = im_proc.im_res(self.orig_frame, self.size)

                self.orig_frame2 = cv2.cvtColor(self.frame2, cv2.cv.CV_BGR2RGB)
                self.orig_frame2 = im_proc.im_mblur(self.orig_frame2)
                self.frame2 = im_proc.im_res(self.orig_frame2, self.size)
                self.frame2 = im_proc.im_gray2rgb(im_proc.im_gray(im_proc.im_mblur(self.frame2)))

                img = QtGui.QImage(self.frame, self.frame.shape[1], self.frame.shape[0], QtGui.QImage.Format_RGB888)
                pix = QtGui.QPixmap.fromImage(img)
                self.image.setPixmap(pix)

                img2 = QtGui.QImage(self.frame2, self.frame2.shape[1], self.frame2.shape[0], QtGui.QImage.Format_RGB888)
                pix2 = QtGui.QPixmap.fromImage(img2)
                self.image2.setPixmap(pix2)
            else:
                self.video_timer.stop()
                self.cap = cv2.VideoCapture(self.n)
                self.start()
        except Exception as e:
            print e.args, e.message

    def start(self):
        self.video_timer = QtCore.QTimer()
        self.video_timer.timeout.connect(self.nextFrameSlot)
        self.video_timer.start(1000./self.fps)
        self.btnOpen.setEnabled(False)

    def stop(self):
        self.video_timer.stop()
        # im_proc.im_save(self.orig_frame, 1)
        # im_proc.im_save(self.orig_frame2, 2)
        self.btnOpen.setEnabled(True)


    def deleteLater(self):
        self.video_timer.stop()
        self.cap.release()
        self.default_image()
        self.btnOpen.setEnabled(True)

