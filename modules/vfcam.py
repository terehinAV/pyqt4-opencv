#!/usr/bin/python
# -*- coding: utf-8 -*-

import cv2


def get_cap(num):
    try:
        cap = cv2.VideoCapture(num)
        if cap is not None:
            ret, frame = cap.read()
            cap_free(cap)
            return frame
        else:
            return None
    except Exception as e:
        print e.args, e.message



def cap_free(cap):
    if cap is not None:
        cap.release()

