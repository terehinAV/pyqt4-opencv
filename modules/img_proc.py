#!/usr/bin/python
# -*- coding: utf-8 -*-

import cv2
import imutils
import datetime as dt
import sys


def im_op(filename):
    img = cv2.imread(filename)
    return img


def im_res(img, k):
    img_res = imutils.resize(img, width=k)
    return img_res


def im_mblur(img, k=5):
    mblur = cv2.medianBlur(img, k)
    return mblur


def im_gray(img):
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    return gray


def im_bgr2rgb(bgr):
    rgb = cv2.cvtColor(bgr, cv2.COLOR_BGR2RGB)
    return rgb


def im_gray2rgb(gray):
    rgb = cv2.cvtColor(gray, cv2.COLOR_GRAY2RGB)
    return rgb


def im_save(img, n=1):
    cur_time = dt.datetime.now()
    cur_data = str(cur_time).split()[0]
    cur_time = str(cur_time).split()[1].split(".")[0].replace(":", "_")
    if str(sys.platform)[0:3] == "lin":
        fname = "saved_images//image{0}_{1}-{2}.jpg".format(str(n), cur_data, cur_time)
    elif str(sys.platform)[0:3] == "win":
        fname = "saved_images\\image{0}_{1}-{2}.jpg".format(str(n), cur_data, cur_time)
    print fname
    cv2.imwrite(fname, img)
