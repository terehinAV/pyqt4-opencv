#!/usr/bin/env python
 
import vtk
 
def MakeVoxel():
    '''
      A voxel is a representation of a regular grid in 3-D space.
    '''
    numberOfVertices = 8
 
    points = vtk.vtkPoints()
    # points.InsertNextPoint(0, 0, 0)
    # points.InsertNextPoint(1, 0, 0)
    # points.InsertNextPoint(0, 1, 0)
    # points.InsertNextPoint(1, 1, 0)
    # points.InsertNextPoint(0, 0, 1)
    # points.InsertNextPoint(1, 0, 1)
    # points.InsertNextPoint(0, 1, 1)
    # points.InsertNextPoint(1, 1, 1)

    points.InsertNextPoint(1, 0, 0)
    points.InsertNextPoint(2, 0, 0)
    points.InsertNextPoint(1, 1, 0)
    points.InsertNextPoint(2, 1, 0)
    points.InsertNextPoint(1, 0, 1)
    points.InsertNextPoint(2, 0, 1)
    points.InsertNextPoint(1, 1, 1)
    points.InsertNextPoint(2, 1, 1)
 
    voxel = vtk.vtkVoxel()
    for i in range(0, numberOfVertices):
        voxel.GetPointIds().SetId(i, i)
 
    ug = vtk.vtkUnstructuredGrid()
    ug.SetPoints(points)
    ug.InsertNextCell(voxel.GetCellType(), voxel.GetPointIds())
 
    return ug

def WritePNG(renWin, fn, magnification=1):
    '''
      Screenshot
 
      Write out a png corresponding to the render window.
 
      :param: renWin - the render window.
      :param: fn - the file name.
      :param: magnification - the magnification.
    '''
    windowToImageFilter = vtk.vtkWindowToImageFilter()
    windowToImageFilter.SetInput(renWin)
    windowToImageFilter.SetMagnification(magnification)
    # Record the alpha (transparency) channel
    # windowToImageFilter.SetInputBufferTypeToRGBA()
    windowToImageFilter.SetInputBufferTypeToRGB()
    # Read from the back buffer
    windowToImageFilter.ReadFrontBufferOff()
    windowToImageFilter.Update()
 
    writer = vtk.vtkPNGWriter()
    writer.SetFileName(fn)
    writer.SetInputConnection(windowToImageFilter.GetOutputPort())
    writer.Write()
 
def DisplayBodies():
 
    titles = list()
    textMappers = list()
    textActors = list()
 
    uGrids = list()
    mappers = list()
    actors = list()
    renderers = list()
 

    uGrids.append(MakeVoxel())
    titles.append('Voxel')

    renWin = vtk.vtkRenderWindow()
    renWin.SetSize(600, 600)
    renWin.SetWindowName('Cell3D Demonstration')
 
    iRen = vtk.vtkRenderWindowInteractor()
    iRen.SetRenderWindow(renWin)
 
    # Create one text property for all
    textProperty = vtk.vtkTextProperty()
    textProperty.SetFontSize(10)
    textProperty.SetJustificationToCentered()
 
    # Create and link the mappers actors and renderers together.
    # for i in range(0, len(uGrids)):
    textMappers.append(vtk.vtkTextMapper())
    textActors.append(vtk.vtkActor2D())

    mappers.append(vtk.vtkDataSetMapper())
    actors.append(vtk.vtkActor())
    renderers.append(vtk.vtkRenderer())

    mappers[0].SetInputData(uGrids[0])
    actors[0].SetMapper(mappers[0])
    renderers[0].AddViewProp(actors[0])

    textMappers[0].SetInput(titles[0])
    textActors[0].SetMapper(textMappers[0])
    textActors[0].SetPosition(0, 0)
    renderers[0].AddViewProp(textActors[0])

    renWin.AddRenderer(renderers[0])

    gridDimensions = 1
    rendererSize = 600
 
    renWin.SetSize(rendererSize * gridDimensions,
                   rendererSize * gridDimensions)
 
    for row in range(0, gridDimensions):
        for col in range(0, gridDimensions):
            index = row * gridDimensions + col
 
            # (xmin, ymin, xmax, ymax)
            viewport = [
                float(col) * rendererSize /
                                     (gridDimensions * rendererSize),
                float(gridDimensions - (row + 1)) * rendererSize /
                                     (gridDimensions * rendererSize),
                float(col + 1) * rendererSize /
                                     (gridDimensions * rendererSize),
                float(gridDimensions - row) * rendererSize /
                                     (gridDimensions * rendererSize)]
 
            if index > len(actors) - 1:
                # Add a renderer even if there is no actor.
                # This makes the render window background all the same color.
                ren = vtk.vtkRenderer()
                ren.SetBackground(.2, .3, .4)
                ren.SetViewport(viewport)
                renWin.AddRenderer(ren)
                continue
 
            renderers[index].SetViewport(viewport)
            renderers[index].SetBackground(.2, .3, .4)
            renderers[index].ResetCamera()
            renderers[index].GetActiveCamera().Azimuth(30)
            renderers[index].GetActiveCamera().Elevation(-30)
            renderers[index].GetActiveCamera().Zoom(0.85)
            renderers[index].ResetCameraClippingRange()
 
    iRen.Initialize()
    renWin.Render()
    return iRen
 
if __name__ == '__main__':
    iRen = DisplayBodies()
    WritePNG(iRen.GetRenderWindow(), "Cell3DDemonstration.png")
    iRen.Start()