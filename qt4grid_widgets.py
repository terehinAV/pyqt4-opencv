#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys
from PyQt4 import QtGui
from PyQt4 import QtCore
import cv2
import modules.img_proc as im_proc

class GridLayout2(QtGui.QWidget):
    def __init__(self, parent=None):
        QtGui.QWidget.__init__(self, parent)

        self.img = cv2.imread(r'img_gray.jpg')
        self.cvImage = self.img
        height, width, bytesPerComponent = self.img.shape
        bytesPerLine = 3 * width
        cv2.cvtColor(self.img, cv2.COLOR_BGR2RGB, self.img)
        QImg = QtGui.QImage(self.img.data, width, height, bytesPerLine,QtGui.QImage.Format_RGB888)
        pixmap = QtGui.QPixmap.fromImage(QImg)
        self.label = QtGui.QLabel('', self)
        self.label.setPixmap(pixmap)
        self.label2 = QtGui.QLabel('', self)
        self.label2.setPixmap(pixmap)




        # По центру экрана
        self.center()

        # Текст окна
        self.filename = ""
        self.setWindowTitle('grid layout')

        # Создаем кнопку с названием
        self.button = QtGui.QPushButton('Open', self)
        self.button.setFocusPolicy(QtCore.Qt.NoFocus)
        # Создаем кнопку с названием
        self.btnMove = QtGui.QPushButton('MoveText', self)
        self.btnMove.setFocusPolicy(QtCore.Qt.NoFocus)

        self.cb = QtGui.QCheckBox('Show title', self)
        self.cb.setFocusPolicy(QtCore.Qt.NoFocus)
        # состояние по умолчанию - выкл
        self.cb.toggle()

        self.btnpushed = QtGui.QPushButton('Push me', self)
        self.btnpushed.setCheckable(True)
        self.btnpushed.setFocusPolicy(QtCore.Qt.NoFocus)

        self.slider = QtGui.QSlider(QtCore.Qt.Horizontal, self)
        self.slider.setFocusPolicy(QtCore.Qt.NoFocus)
        # self.slider.setGeometry(30, 40, 100, 30)

        # прогрессбар
        self.pbar = QtGui.QProgressBar(self)
        self.pbar.setGeometry(30, 40, 200, 25)

        self.btnstart = QtGui.QPushButton('Start', self)
        self.btnstart.setCheckable(True)
        self.btnstart.setFocusPolicy(QtCore.Qt.NoFocus)

        self.timer = QtCore.QBasicTimer()
        self.step = 0

        # self.widget = QtGui.QWidget(self)
        # self.widget.setStyleSheet("QWidget { background-color: %s }"
        #     % "#000000")
        # self.widget.setGeometry(130, 22, 100, 100)



        # Связываем событие нажатия первой кнопки с методом
        self.connect(self.button, QtCore.SIGNAL('clicked()'), self.showDialog)
        self.setFocus()
        # Связываем событие нажатия второй кнопки с методом
        self.connect(self.btnMove, QtCore.SIGNAL('clicked()'), self.movetext)
        self.setFocus()

        self.connect(self.slider, QtCore.SIGNAL('valueChanged(int)'), self.slidervalue)
        self.setFocus()

        # Создаем лейблы
        #self.review = QtGui.QLabel('Review')
        #self.about = QtGui.QLabel('about')
        #
        # Создаем редактируемые поля
        self.reviewEdit = QtGui.QTextEdit()
        self.aboutEdit = QtGui.QTextEdit()
        # чек бокс
        self.connect(self.cb, QtCore.SIGNAL('stateChanged(int)'), self.changeTitle)
        # прогрессбар кнопка старта
        self.connect(self.btnstart, QtCore.SIGNAL('clicked()'), self.onStart)




        # Создаем сетку для размещения виджетов
        grid = QtGui.QGridLayout()
        grid.setSpacing(10)
        # Размещаем первый столбец
        grid.addWidget(self.button, 1, 2)
        # Размещаем второй столбец
        grid.addWidget(self.btnMove, 2, 2)
        # Размещаем третий столбец
        # grid.addWidget(self.review, 1, 0)
        # Редактор занимает 5 строк и 1 столбец и начинается с третьей строки 1 столбца
        grid.addWidget(self.reviewEdit, 1, 1, 2, 1)
        grid.addWidget(self.btnpushed, 3, 2)
        # Изображение
        grid.addWidget(self.label, 1, 3, 5, 1)
        grid.addWidget(self.label2, 1, 4, 5, 1)
        # Размещаем первый столбец
        # grid.addWidget(self.about, 6, 0)
        grid.addWidget(self.aboutEdit, 6, 1, 10, 2)
        grid.addWidget(self.cb, 16, 0)

        grid.addWidget(self.slider, 17, 0)
        grid.addWidget(self.pbar, 18, 0)
        grid.addWidget(self.btnstart, 18, 1)

        # grid.addWidget(self.widget, 19, 0)




        self.setLayout(grid)
        self.resize(350, 300)

    def center(self):
        screen = QtGui.QDesktopWidget().screenGeometry()
        size =  self.geometry()
        self.move((screen.width()-size.width())/2, (screen.height()-size.height())/2)

    def showDialog(self):
        # !!!!!!!! отловить ошибку ascii если символы русские в пути к файлу. возвращать название файла


        self.filename = QtGui.QFileDialog.getOpenFileName(self, 'Open file',
                    '/home')



        print str(self.filename)
        if self.filename != "":
            print self.filename
            # self.cvImage = im_proc.im_op(u'ff1.jpg', 1)
            # if self.cvImage is not None:
            fn = str(self.filename).split("/")
            fn2 = str(self.filename).replace("/", "//")
            print fn2
            if fn[-1] != "" or fn[-1] is not None:
                pm = self.on_pixmap(fn2)
                self.label.setPixmap(pm)

            #self.img = cv2.imread(r'{0}'.format(self.filename))
            file=open(self.filename)
            data = file.read()
            self.aboutEdit.setText(self.filename)
            self.cb.toggle()
            # self.textEdit.setText(data)

    def on_pixmap(self, fname):
        # !!!!!!!!!!!! проблема с форматом РГБ888
        if fname != "":
            cvImage = im_proc.im_op(fname)
            if cvImage is not None:
                h, w, bpLine = cvImage.shape
                bpLine = 3 * w
                cv2.imshow("1", cvImage)
                gray = cv2.cvtColor(cvImage, cv2.COLOR_BGR2GRAY)
                bgr = cv2.cvtColor(gray, cv2.COLOR_GRAY2BGR)
                cv2.imshow("2", bgr)

                qimage = QtGui.QImage(bgr.data, w, h, bpLine, QtGui.QImage.Format_RGB888)
                pixm = QtGui.QPixmap.fromImage(qimage)
                return pixm



    def movetext(self):
        self.text = self.aboutEdit.toPlainText()
        self.reviewEdit.setText(self.text)
        self.aboutEdit.setText("")

    def changeTitle(self, value):
        if self.cb.isChecked():
            if self.filename is not None:
                self.setWindowTitle(self.filename)
            else:
                self.setWindowTitle('Checkbox')
        else:
            self.setWindowTitle('')

    def slidervalue(self, value):
        pos = self.slider.value()
        self.aboutEdit.setText(str(pos))




    def timerEvent(self, event):
        if self.step >= 100:
            self.timer.stop()
            return
        self.step = self.step + 1
        self.pbar.setValue(self.step)

    def onStart(self):
        if self.timer.isActive():
            self.timer.stop()
            self.btnstart.setText('Start')
        elif self.timer.isActive() == False and self.step != 100:
            self.timer.start(100, self)
            self.btnstart.setText('Stop')
        else:
            self.step = 0
            self.btnstart.setText('Start')

    def ipl2qimg(self):
        pass


    '''def paintEvent(self, QPaintEvent):
        painter = QtGui.QPainter()
        painter.begin(self)
        painter.drawImage(0, 0, self.mQImage)
        painter.end()
    '''


app = QtGui.QApplication(sys.argv)
qb = GridLayout2()
qb.show()
sys.exit(app.exec_())
